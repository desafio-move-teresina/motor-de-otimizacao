from django.db import models
import uuid

class MotorResult(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    fo = models.IntegerField()

    def __str__(self):
        return str(self.fo)


class Polygon(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    lat = models.FloatField()
    long = models.FloatField()

    def __str__(self):
        return self.id

class BusStops(models.Model):
    id = models.IntegerField(primary_key=True)
    lat = models.FloatField()
    long = models.FloatField()
    fixed = models.IntegerField()

    def __str__(self):
        return self.id

class Data(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    routes = models.IntegerField()
    polygon = models.ForeignKey(Polygon, null = True, on_delete=models.SET_NULL)
    busStops = models.ForeignKey(BusStops, null = True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.id

