from django.db.models import fields
from rest_framework import serializers
from .models import MotorResult, Data, Polygon, BusStops

class MotorResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = MotorResult
        fields = ('id','fo')


class DataSerializer(serializers.ModelSerializer):
    my_list_polygon = serializers.ListField(child=serializers.DictField())
    my_list_busStop = serializers.ListField(child=serializers.DictField())
    
    class Meta:
        fields = ('id','routes','polygon','busStops')
        model = Polygon, BusStops
      