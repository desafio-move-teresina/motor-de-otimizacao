from typing import Protocol
import uuid
from .models import  Data
from .tasks import engine
from rest_framework import viewsets
from .motorResult import DataSerializer
from rest_framework.response import Response

class MotorResultViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer

    def list(self, response):
        return Response('Hello World')

    def create(self, response):

        protocol=uuid.uuid4()
        engine.delay(uuid.uuid4(), response.data)
        return Response( {"protocol":protocol} )




