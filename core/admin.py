from django.contrib import admin
from core.models import MotorResult

class MotorResultAdmin(admin.ModelAdmin):
    list_display = ('id','fo')
    list_display_links = ('id','fo')
    search_fields = ('fo',)

admin.site.register(MotorResult,MotorResultAdmin)