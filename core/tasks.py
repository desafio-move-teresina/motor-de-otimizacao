from __future__ import absolute_import, unicode_literals
from celery import shared_task
from engine.main import main

@shared_task
def engine(protocolo, data):
    return main(protocolo, data)