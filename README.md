# Optime

Desenvolvimento **optimization service (serviço de otimização)** para a aplicação de gerenciamento das empresas, ônibus, mapas, pontos de ônibus e rotas para a prefeitura de Teresina.

As tecnologias usadas são [Python](https://www.python.org/), [django](https://www.djangoproject.com/), e [RabbitMQ](https://www.rabbitmq.com/)

## Links

### Projeto

- [Bitbucket](https://bitbucket.org/desafio-move-teresina/motor-de-otimizacao/src/master/) - repositório do projeto no Bitbucket
- [Jira](https://optime.atlassian.net/jira/software/projects/OPT/boards/1) - gerenciamento do projeto no Jira
- [Confluence](https://optime.atlassian.net/wiki/spaces/OPT/overview) - documentação do projeto no Confluence

### Docs e outros

- [Python](https://www.python.org/doc/) - documentação do Python
- [Celery](https://docs.celeryproject.org/en/stable/) - fila de tarefas distribuídas 
- [RabbitMQ](https://www.rabbitmq.com/documentation.html) - documentação para o RabbitMQ
- [django](https://docs.djangoproject.com/en/4.0/) - documentação para o django


## Intalação das Depedências

1 - django:
```bash
pip install djangorestframework
```

2 - Celery :
```bash
pip install celery
```

3 - RabbitMQ (Ubuntu Linux 20.04LTS)
```bash
sudo apt-get install rabbitmq-server
```

## Configurar e executar o projeto localmente

Primeiro instale levante o serviço de filas com celery e RabbitMQ:

```bash
celery -A serve_motor worker --loglevel=INFO --without-gossip --without-mingle --without-heartbeat -Ofair --pool=solo
```

Depois, execute o projeto:

```bash
python3 manage.py runserver
```

O servidor web será iniciado em [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

## Versões

O projeto está sendo testado usando ferramentas nas seguintes versões:

```bash
python3 --version
Python 3.8.10

celery --version
5.2.1 (dawn-chorus)

sudo rabbitmqctl status | grep -i "version"
RabbitMQ version: 3.8.2

pip3 freeze
certifi==2021.10.8
charset-normalizer==2.0.7
click==8.0.3
colorama==0.4.4
cspy==1.0.1
cycler==0.11.0
Deprecated==1.2.13
fonttools==4.28.1
idna==3.3
kiwisolver==1.3.2
matplotlib==3.5.0
networkx==2.6.3
numpy==1.21.4
packaging==21.2
Pillow==8.4.0
PuLP==2.5.1
pyparsing==2.4.7
PyQt5==5.15.6
PyQt5-Qt5==5.15.2
PyQt5-sip==12.9.0
python-dateutil==2.8.2
python-tsp==0.2.1
requests==2.26.0
setuptools-scm==6.3.2
Shapely==1.8.0
six==1.16.0
tabulate==0.8.9
tomli==1.2.2
tqdm==4.62.3
tsplib95==0.7.1
urllib3==1.26.7
vrpy==0.5.1
wrapt==1.13.3
```
