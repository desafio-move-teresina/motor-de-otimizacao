from .get_dados import dados
from .convet import informations
from .cover_algorithm import cobertura_v2
from .matrix_distence import get_matrix_distance
from .only_algorithm_route import algorithm_only_route
from .end_points import salve_results
from .routing_algorithm_multiple_routes import multiple_routes

def get_init(protocolo,polygon, busStops):

    print("Inicio_Processo_"+protocolo)
    return dados(polygon, busStops)
  
def get_result_cover(init,ploat):

    print("Inicio_Processo_Cobertura")
    return cobertura_v2(init["C"],init["F"],init["dmin"],init["P"],init["flag"], ploat)

def get_result_routes(result_cover, date, routes):
   
    if(routes == 1):
        print("Inicio_Processo_Contrução_Matriz_Distância")
        matarix_distence = get_matrix_distance(result_cover, date, 1)

        print("Inicio_Processo_de_Roterizacão")
        result_routes = algorithm_only_route(result_cover, date ,matarix_distence, 0)

    else:
        print("Inicio_Processo_Contrução_Matriz_Distância")
        matarix_distence,size = get_matrix_distance(result_cover, date, 2)

        print("Inicio_Processo_de_Roterizacão")
        result_routes = multiple_routes(matarix_distence, routes, size, date, result_cover,0)
    
    return result_routes

def salve(result_routes):
    salve_results(result_routes)

def main(protocolo, json):

    try:
        routes, polygon, busStops = informations(json)
        date=get_init(protocolo, polygon, busStops)
        result_cover=get_result_cover(date, 0)
        result_routes=get_result_routes(result_cover, date, routes)
    except Exception as e:
        print("Erro na socilitação de otimização, motivo: ",e)
        result_routes = []

    salve(result_routes)
    return 0
    