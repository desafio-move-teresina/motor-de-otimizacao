from networkx.algorithms import cluster
from vrpy import VehicleRoutingProblem
from .ploat import grafico_roteiro_n
import numpy as np

def multiple_routes(G,routes,size, init, resultado_cobertura, ploat):
    index_cluster = resultado_cobertura['pos_cluster']
    cluster = resultado_cobertura['cluster']

    prob = VehicleRoutingProblem(G)
    prob.num_vehicles = routes
    prob.use_all_vehicles = True
    prob.solve()

    if(ploat==1):
        grafico_roteiro_n(prob.best_routes, size)

    result =prob.best_routes

    name = []
    for i in range( np.size(cluster,0) ):
        name.append(int(init["pos_real"][index_cluster][i]))

    best_points_index = []
    for i in result:
        line = []
        result[i][0] = 0
        result[i][-1] = 0
        for j in range( len(result[i]) -1 ):
            line.append(int(name[result[i][j]]))
        best_points_index.append(line)

    return np.array(best_points_index) 