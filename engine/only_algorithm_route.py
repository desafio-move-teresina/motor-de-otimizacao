import numpy as np
from .ploat import grafico_roteiro
from python_tsp.heuristics import solve_tsp_simulated_annealing

def algorithm_only_route(resultado_cobertura, init ,matrix, ploat):
    cluster = resultado_cobertura['cluster']
    index_cluster = resultado_cobertura['pos_cluster']

    sources = np.copy(cluster)
    permutation, distance = solve_tsp_simulated_annealing(matrix)
    best_points_coordinate = sources[permutation, :]
    best_points_index = index_cluster[permutation]

    index = []
    for i in range(np.size(cluster,0)):
        index.append(int(init["pos_real"][best_points_index][i]))
    
    if(ploat==1):
        grafico_roteiro(best_points_coordinate)
  
    return [index]