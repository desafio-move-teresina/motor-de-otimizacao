from os import name
from networkx import DiGraph, from_numpy_matrix, relabel_nodes, set_node_attributes
import networkx as nx
import numpy as np
from vrpy import VehicleRoutingProblem
import math
from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt

data = np.loadtxt('data/eil51.tsp.txt', usecols=[1,2])
#data = data[0:24,:]
num_rotas = 7


def matrix_distance(A,B):
    dist = lambda p1, p2: math.sqrt(((p1-p2)**2).sum())
    DM = np.asarray([[dist(p1, p2) for p2 in A] for p1 in B])
    return DM

DM = matrix_distance(data,data)
DM_aux = []
for k in range(len(DM)):
    DM_aux.append(np.roll(DM[k,:],-1))

DM_alterada = np.zeros((len(DM)+1,len(DM)+1))
DM_alterada[0:len(DM),1:len(DM)+1] = DM_aux
DEMAND = {}
for i in range(1,len(data)):
    DEMAND[i] = 1

A = np.array(DM_alterada, dtype=[("cost", int)])
G = from_numpy_matrix(A, create_using=nx.DiGraph())

#set_node_attributes(G, values=DEMAND, name="demand")
G = relabel_nodes(G, {0: "Source", len(data): "Sink"})

#prob = VehicleRoutingProblem(G, load_capacity=math.ceil(len(data)/num_rotas))
prob = VehicleRoutingProblem(G)
prob.num_vehicles = num_rotas
prob.use_all_vehicles = True
prob.solve()
prob.best_value
rotas = prob.best_routes

def plot_rota(rotas):
    for rota in rotas:
        rotas[rota][0] = 0
        rotas[rota][-1] = 0
        best_points_coordinate = data[rotas[rota], :]
        #best_points_coordinate = np.concatenate([best_points_coordinate, [best_points_coordinate[0,:]]])
        plt.plot(best_points_coordinate[:, 0], best_points_coordinate[:, 1],
            marker='o', markerfacecolor='b', linestyle='-',)
        plt.plot(best_points_coordinate[0, 0], best_points_coordinate[0, 1],
            marker='o', markerfacecolor='w', linestyle='-',)
    plt.show()

plot_rota(rotas)
