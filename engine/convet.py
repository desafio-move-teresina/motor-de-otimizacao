
def informations(data):
    routes = int(data['routes'])
    polygon = []
    for polygon_i in data['polygon']:
        line = []
        line.append(float(polygon_i['lat']))
        line.append(float(polygon_i['long']))
        polygon.append(line)

    busStops = []
    for busStops_i in data['busStops']:
        line = []
        line.append(int(busStops_i['id']))
        line.append(float(busStops_i['lat']))
        line.append(float(busStops_i['long']))
        line.append(int(busStops_i['fixed']))
        busStops.append(line)
    
    return routes, polygon, busStops
