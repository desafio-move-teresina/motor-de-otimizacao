from .end_points import decision_variables
import numpy as np

def dados(poligon,cluster):
    flag = 1
    decision =  decision_variables()
    P = np.array(poligon)
    C = np.array(cluster)
    pos_real = C[:,0]
    ponto_fixo = np.array(list(map(bool,C[:,3])))
    C = C[:,1:3]
    dmin =float(decision['defaultBusStopRadius'])/(111.32/0.001)
    F = C[ponto_fixo,:]

    return{
        "C":C,
        "P":P,
        "F":F.reshape((1,2)),
        "flag":flag,
        "dmin":dmin,
        "pos_real": pos_real
    }