import matplotlib.pyplot as plt
import matplotlib as pltColor
import numpy as np

def grafico(C,X,cluster,P):

    x_1, y_1 = C.T
    x_2, y_2 = X.T
    x_3, y_3 = cluster.T

    plt.gca().add_patch(plt.Polygon(P, fill = None, lw = 3., ls = 'dotted', edgecolor = 'k'))
    plt.scatter(x_1, y_1,color='red')
    plt.scatter(x_2, y_2,color='blue')
    plt.scatter(x_3, y_3,color='black')
       
    plt.grid(True)
    plt.axis('scaled')
    plt.show()

def grafico_roteiro(best_points_coordinate):
    best_points_coordinate = np.concatenate([best_points_coordinate, [best_points_coordinate[0,:]]])
    plt.plot(best_points_coordinate[:, 0], best_points_coordinate[:, 1],
            marker='o', markerfacecolor='b', color='c', linestyle='-')

    plt.show()

def grafico_roteiro_n(rotas, data):

    for rota in rotas:
        rotas[rota][0] = 0
        rotas[rota][-1] = 0
        best_points_coordinate =data[ rotas[rota], :]
        #best_points_coordinate = np.concatenate([best_points_coordinate, [best_points_coordinate[0,:]]])
        plt.plot(best_points_coordinate[:, 0], best_points_coordinate[:, 1],marker='o', markerfacecolor='b', linestyle='-',)
        plt.plot(best_points_coordinate[0, 0], best_points_coordinate[0, 1],marker='o', markerfacecolor='w', linestyle='-',)
    plt.show()