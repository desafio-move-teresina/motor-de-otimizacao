import numpy as np
from networkx import from_numpy_matrix, relabel_nodes
import networkx as nx
from .end_points import get_distance_data
from geopy.distance import geodesic

def seleciona_indice_matrix_distance(idx_inicial,lat_inicial,long_inicial,idx_final,lat_final,long_final):

    dist_euclidiana_coord = geodesic([lat_inicial,long_inicial],[lat_final,long_final]).km * 1000
    if dist_euclidiana_coord < 50 or dist_euclidiana_coord > 1000:
        return {"distance":dist_euclidiana_coord,"duration":0}                      
    else:
        return get_distance_data(idx_inicial,lat_inicial,long_inicial,idx_final,lat_final,long_final)

def real_distance(resultado_cobertura, init):
    cluster = resultado_cobertura['cluster']
    pos_cluster = resultado_cobertura['pos_cluster']
    latitude = init["C"][:,0][pos_cluster]
    longitude = init["C"][:,1][pos_cluster]

    DM = np.zeros((np.size(cluster,0),np.size(cluster,0)))
    TM = np.zeros((np.size(cluster,0),np.size(cluster,0)))

    index_matriz = []
    for i in range(np.size(cluster,0)):
        index_matriz.append(int(init["pos_real"][pos_cluster][i]))
        for j in range(np.size(cluster,0)):
            idx_inicial = int(init["pos_real"][pos_cluster][i])
            idx_final = int(init["pos_real"][pos_cluster][j])

            if idx_inicial != idx_final:
                lat_inicial = latitude[i]
                lat_final = latitude[j]
                long_inicial = longitude[i]
                long_final = longitude[j]

                resultado_get = seleciona_indice_matrix_distance(idx_inicial,lat_inicial,long_inicial,idx_final,lat_final,long_final)
                
                DM[i,j] = (resultado_get['distance'])
                TM[i,j] = (resultado_get['duration'])
    return DM

def coordinates_to_adjacency_matrix(resultado_cobertura):
    data = resultado_cobertura['cluster']

    a = np.zeros((len(data),len(data)))
    for i in range(len(a)):
        for j in range(len(a)):
            if not i == j:
                a[i][j] = np.linalg.norm(data[i] - data[j])
    return a

def matrix_distence_diGraph(resultado_cobertura, init):
    
    DM = real_distance(resultado_cobertura, init)
    cluster = resultado_cobertura['cluster']
    data = np.copy(cluster)

    DM_aux = []
    for k in range(len(DM)):
        DM_aux.append(np.roll(DM[k,:],-1))

    DM_alterada = np.zeros((len(DM)+1,len(DM)+1))
    DM_alterada[0:len(DM),1:len(DM)+1] = DM_aux
    DEMAND = {}
    for i in range(1,len(data)):
        DEMAND[i] = 1

    A = np.array(DM_alterada, dtype=[("cost", float)])
    G = from_numpy_matrix(A, create_using=nx.DiGraph())

    G = relabel_nodes(G, {0: "Source", len(data): "Sink"})

    return G, data

def get_matrix_distance(resultado_cobertura, init, euclidean):
    if (euclidean == 1):
        return real_distance(resultado_cobertura, init)
    if (euclidean == 2):
        return matrix_distence_diGraph(resultado_cobertura, init)
    else:
        return coordinates_to_adjacency_matrix(resultado_cobertura)