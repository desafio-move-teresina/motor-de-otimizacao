import requests
import json
from .data import URL

def decision_variables():
    decision_variables_request = requests.get(url=URL+"/api/decision-variables", headers=dict(Referer=URL))
    decision_variables = decision_variables_request.json()
    return {
        "stopMinimumDistance": decision_variables['stopMinimumDistance'],
        "stopMaximumDistance": decision_variables['stopMaximumDistance'],
        "maximumWait": decision_variables['stopMinimumDistance'],
        "defaultBusStopRadius": decision_variables['defaultBusStopRadius'],
    }

def get_distance_data(code_Origin, latitude_Origin, longitude_Origin, code_Destination, latitude_Destination, longitude_Destination):

    point = [{
        "origin": {
            "code" : code_Origin,
            "latitude" : latitude_Origin,
            "longitude" : longitude_Origin
        },
        "destination" : {
            "code" : code_Destination,
            "latitude" : latitude_Destination,
            "longitude" : longitude_Destination
        }
    }]
    
    get_distance_data_request = requests.post(url=URL+"/api/getDistanceData",data=json.dumps(point),headers=dict(Referer=URL))
    get_distance_data = get_distance_data_request.json()

    return {
        "origin_prodater_id": get_distance_data['origin_prodater_id'],
        "destination_prodater_id": get_distance_data['destination_prodater_id'],
        "distance": get_distance_data['distance'],
        "duration": get_distance_data['duration'],
    }

def salve_results(results):

    rotas = []
    for i in range(len(results)):
        busStop = []
        for j in range(len(results[i])):
            code = {
                "code": int(results[i][j])
            }
            busStop.append(code) 
        busStops = {
            "busStops": busStop
        }
        rotas.append(busStops)

    body = {
        "routes": rotas
    }
    print(body)
    requests.post(url=URL+"/api/engine", json=body, headers=dict(Referer=URL))

