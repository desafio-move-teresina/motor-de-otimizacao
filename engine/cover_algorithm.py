import math
import sys
import numpy as np
from .ploat import grafico
from shapely.geometry import Polygon

def matrix_distance(A,B):
    dist = lambda p1, p2: math.sqrt(((p1-p2)**2).sum())
    DM = np.asarray([[dist(p1, p2) for p2 in A] for p1 in B])
    return DM

def separa_cluster(C,X,P, ploat):

    D = matrix_distance(C,X)
    pos_cluster = np.argmin(D,axis=1)
    cluster = C[pos_cluster]

    if(ploat==1):
        grafico(C,X,cluster,P)

    return {
        "cluster":np.array(cluster),
        "pos_cluster":np.array(pos_cluster)
    }

def knanmean_cobertura(C, F, k , flag):

    aux = np.nanmean(C,0)

    n=0
    if( np.size(C,0) != 0):
        n = np.size(C,0)

    r=0
    if( np.size(F,0) != 0):
        r = np.size(F,0)

    if r == 0:
        F = np.empty(shape=(0, 2))
    if r == k:
        cluster = F
    else:
        arr=np.random.permutation( (n) - r) + 1
        cluster_aux = []
        for i in range(k-r):
            cluster_aux.append( C[ arr[i] ] )
        cluster = np.concatenate( (F,np.array(cluster_aux) ) )

    cluster_anterior = np.zeros(shape=(k,2))
    Pos = {}
    it = 0

    while( sum(sum(abs(cluster_anterior - cluster))) > 10**(-8) ) :
        it = it + 1
        D = []
        C_max = np.size(C,0)
        for C_i in range( 0 , C_max ):
            cluster_j_max = np.size(cluster,0)
            linha = []
            for cluster_j in range( 0 , cluster_j_max ):
                distancia = math.dist( C[C_i], cluster[cluster_j])
                linha.append(distancia)
            D.append(linha)
    
        D = np.array(D)
        pos_cluster = []
        D_i_max = np.size(D,0)
        for D_i in range( 0 , D_i_max ):
            minValue = np.amin(D[D_i])
            minIndex = np.where( D[D_i] == minValue)
            pos_cluster.append( minIndex[0][0] )
        
        pos_cluster = np.array(pos_cluster)
        cluster_anterior = np.copy(cluster)        
    
        for i in range(0,k):
            aux_pos = np.where( pos_cluster == i)

            C_pos = C[aux_pos] 
            media = np.nanmean(C_pos,0)
            Pos[i] = aux_pos
            
            if i > r :
                
                if math.isnan(sum(media)):
                    cluster[i] = np.random.rand()*aux
                else :
                    cluster[i] = media
    
        cluster_real = np.copy(cluster)
        if flag == 2 :
            print("==")

    return {
        "cluster": cluster,
        "D": D,
        "Pos":Pos,
        "cluster_real": cluster_real,
        "it":it
    }

def cobertura_v2(C,F,dmin,P,flag,ploat):
    x, y = P.T
    pgon = Polygon(zip(x, y))
    area_pol = pgon.area
    k = 1

    raio_cobertura = sys.maxsize
    r = 0
    if( np.size(F,0) != 0):
        r = np.size(F,0)
    var_raio = []

    if (r > k) and (r != 0):
        k = r
    
    Pos = 0
    cluster = 0
    cluster_real = 0

    while raio_cobertura > dmin:

        raios = np.zeros(shape=(k,1))
        kmaean = knanmean_cobertura(C,F,k,flag)
        it = kmaean["it"]
        Pos = kmaean["Pos"]
        D = kmaean["D"]
        cluster = kmaean["cluster"]
        cluster_real = kmaean["cluster_real"]

        for j in range(0,k):
            pos = Pos[j]
            raios[j] = np.amax(D[pos,j])
            
        raio_cobertura = np.nanmean(raios)
        var_raio = np.concatenate((var_raio, (k, raio_cobertura))) 

        if raio_cobertura > dmin:
            k_possivel = k + max(math.floor(k*(raio_cobertura/dmin - 1)),1)
            if( k_possivel < np.size(C,0)):
                k = k_possivel
            else:
                k=k+1

    return separa_cluster(C, cluster, P, ploat)
